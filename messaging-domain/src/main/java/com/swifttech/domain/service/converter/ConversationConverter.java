package com.swifttech.domain.service.converter;

import com.swifttech.domain.document.ConversationDocument;
import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.document.type.MessageType;
import com.swifttech.domain.dto.ConversationCreateDto;
import com.swifttech.domain.dto.ConversationDto;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConversationConverter {

  private final MapperFacade mapperFacade;

  public ConversationDocument convertToDocument(final MessageDocument messageDocument) {
    final ConversationDocument conversationDocument = new ConversationDocument();

    conversationDocument.setConversationId(UUID.randomUUID().toString());
    conversationDocument.setUserId(messageDocument.getSenderId());
    conversationDocument.setCreatedAt(LocalDateTime.now());

    if (messageDocument.getType() == MessageType.DIRECT) {
      conversationDocument.setInterlocutorId(messageDocument.getReceiverId());
    } else if (messageDocument.getType() == MessageType.GROUP) {
      conversationDocument.setGroupId(messageDocument.getGroupId());
    }

    return conversationDocument;
  }

  public ConversationDocument convertToDocument(final ConversationCreateDto conversationCreateDto) {
    final ConversationDocument conversationDocument = new ConversationDocument();

    conversationDocument.setConversationId(UUID.randomUUID().toString());
    conversationDocument.setUserId(conversationCreateDto.getUserId());
    conversationDocument.setCreatedAt(LocalDateTime.now());
    conversationDocument.setInterlocutorId(conversationCreateDto.getInterlocutorId());
    conversationDocument.setGroupId(conversationCreateDto.getGroupId());

    return conversationDocument;
  }

  public ConversationDto convertToDto(final ConversationDocument conversationDocument) {
    return mapperFacade.map(conversationDocument, ConversationDto.class);
  }

}
