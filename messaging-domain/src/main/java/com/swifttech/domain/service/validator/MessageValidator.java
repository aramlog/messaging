package com.swifttech.domain.service.validator;

import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.exception.ErrorCode;
import com.swifttech.domain.exception.MessageException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageValidator {

  public void validateStoreMessage(MessageCreateDto messageCreateDto) {
    if (messageCreateDto == null) {
      throw new MessageException(ErrorCode.MESSAGE_NOT_PROVIDED).logError();
    }

    if (StringUtils.isEmpty(messageCreateDto.getReceiverId())
        && StringUtils.isEmpty(messageCreateDto.getGroupId())) {
      throw new MessageException(
          ErrorCode.MESSAGE_NOT_PROVIDED, "Please provide either receiver id or group id.").logWarn();
    }

    if (StringUtils.isEmpty(messageCreateDto.getText())
        && StringUtils.isEmpty(messageCreateDto.getImage())) {
      throw new MessageException(
          ErrorCode.MESSAGE_NOT_PROVIDED, "Please provide either message text or image.").logWarn();
    }
  }

  public void validateSender(String senderId) {
    throw new MessageException(ErrorCode.MESSAGE_INAPPROPRIATE_SENDER).logWarn();
  }
}
