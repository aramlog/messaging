package com.swifttech.domain.service.converter;

import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.document.embedded.BodyEmbedded;
import com.swifttech.domain.document.embedded.HeaderEmbedded;
import com.swifttech.domain.document.type.MessageType;
import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.dto.MessageDto;
import com.swifttech.domain.exception.ErrorCode;
import com.swifttech.domain.exception.MessageException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@AllArgsConstructor
public class MessageConverter {

  private final MapperFacade mapperFacade;

  public MessageDocument convertToDocument(final MessageCreateDto messageDto) {
    final MessageDocument messageDocument = new MessageDocument();
    if (messageDto.getExtId() == null) {
      messageDocument.setExtId(UUID.randomUUID().toString());
    }
    messageDocument.setReceiverId(messageDto.getReceiverId());
    messageDocument.setGroupId(messageDto.getGroupId());
    messageDocument.setCreatedAt(LocalDateTime.now());
    messageDocument.setType(convertToType(messageDto.getReceiverId(), messageDto.getGroupId()));
    messageDocument.setHeader(convertToHeader(messageDto.getHeader()));
    messageDocument.setBody(convertToBody(messageDto));
    messageDocument.setDeleted(false);

    return messageDocument;
  }

  public MessageDto convertToDto(final MessageDocument message) {
    final MessageDto messageDto = mapperFacade.map(message, MessageDto.class);
    return messageDto;
  }

  private Set<HeaderEmbedded> convertToHeader(final Map<String, String> headerMap) {
    final Set<HeaderEmbedded> header = new HashSet<>();
    if (headerMap != null) {
      for (Map.Entry<String, String> entry : headerMap.entrySet()) {
        header.add(HeaderEmbedded.builder()
            .key(entry.getKey())
            .value(entry.getValue())
            .build());
      }
    }
    return header;
  }

  private BodyEmbedded convertToBody(final MessageCreateDto messageCreateDto) {
    final BodyEmbedded body = new BodyEmbedded();
    if (!StringUtils.isEmpty(messageCreateDto.getText())) {
      body.setContent(messageCreateDto.getText());
    }
    if (!StringUtils.isEmpty(messageCreateDto.getImage())) {
      body.setImage(messageCreateDto.getImage());
    }
    return body;
  }

  private MessageType convertToType(String receiverId, String groupId) {
    if (StringUtils.isEmpty(receiverId) && !StringUtils.isEmpty(groupId)) {
      return MessageType.GROUP;
    }
    if (!StringUtils.isEmpty(receiverId) && StringUtils.isEmpty(groupId)) {
      return MessageType.DIRECT;
    }
    throw new MessageException(ErrorCode.MESSAGE_INVALID, "Please provide either receiver id or group id.").logWarn();
  }
}
