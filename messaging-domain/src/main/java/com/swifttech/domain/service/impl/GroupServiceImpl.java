package com.swifttech.domain.service.impl;

import com.swifttech.domain.SecurityHolder;
import com.swifttech.domain.document.GroupDocument;
import com.swifttech.domain.document.embedded.MemberEmbedded;
import com.swifttech.domain.document.type.MemberType;
import com.swifttech.domain.dto.GroupCreateDto;
import com.swifttech.domain.dto.GroupDto;
import com.swifttech.domain.dto.MemberDto;
import com.swifttech.domain.exception.ErrorCode;
import com.swifttech.domain.exception.GroupException;
import com.swifttech.domain.repository.GroupRepository;
import com.swifttech.domain.service.GroupService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

  private final GroupRepository groupRepository;
  private final MapperFacade mapper;
  private final SecurityHolder securityHolder;

  @Override
  public GroupDto createGroup(final GroupCreateDto groupCreateDto) {
    if (groupCreateDto.getExtId() != null && groupRepository.findByExtId(groupCreateDto.getExtId()).isPresent()) {
      throw new GroupException(ErrorCode.GROUP_EXT_ID_DUPLICATED).logError();
    }

    final GroupDocument groupDocument = mapper.map(groupCreateDto, GroupDocument.class);
    groupDocument.addMember(MemberEmbedded.asSelfAdmin(securityHolder.getContextUserId()));
    groupDocument.setExtId(UUID.randomUUID());
    groupDocument.setCreatedAt(LocalDateTime.now());
    groupDocument.setCreatedBy(securityHolder.getContextUserId());

    final GroupDocument groupDocumentSaved = groupRepository.save(groupDocument);
    return mapper.map(groupDocumentSaved, GroupDto.class);
  }

  @Override
  public void addMember(final UUID groupExtId, final String userId, final MemberType memberType) {
    final GroupDocument groupDocument = findByExtId(groupExtId);
    addMember(groupDocument, userId, memberType);
  }

  @Override
  public void addMember(final String groupId, final String userId, final MemberType memberType) {
    final GroupDocument groupDocument = findById(groupId);
    addMember(groupDocument, userId, memberType);
  }

  @Override
  public void removeMember(final UUID groupExtId, final String userId) {
    final GroupDocument groupDocument = findByExtId(groupExtId);
    removeMember(groupDocument, userId);
  }

  @Override
  public void removeMember(final String groupId, final String userId) {
    final GroupDocument groupDocument = findById(groupId);
    removeMember(groupDocument, userId);
  }

  @Override
  public List<MemberDto> groupMembers(UUID groupExtId) {
    final GroupDocument groupDocument = findByExtId(groupExtId);
    return groupMembers(groupDocument);
  }

  @Override
  public List<MemberDto> groupMembers(String groupId) {
    final GroupDocument groupDocument = findById(groupId);
    return groupMembers(groupDocument);
  }

  private void addMember(GroupDocument groupDocument, String userId, MemberType memberType) {
    if (memberType == MemberType.ADMIN) {
      groupDocument.addMember(MemberEmbedded.asAdmin(userId, securityHolder.getContextUserId()));
    } else if (memberType == MemberType.PARTICIPANT) {
      groupDocument.addMember(MemberEmbedded.asParticipant(userId, securityHolder.getContextUserId()));
    }
    groupRepository.save(groupDocument);
  }

  private void removeMember(final GroupDocument groupDocument, final String userId) {
    MemberEmbedded memberEmbedded = groupDocument.getMembers().stream()
        .filter(member -> userId.equals(member.getUserId()))
        .findFirst().orElseThrow(() -> new GroupException(ErrorCode.GROUP_MEMBER_NOT_FOUND).logError());

    memberEmbedded.setRemovedBy(securityHolder.getContextUserId());
    memberEmbedded.setRemovedAt(LocalDateTime.now());

    groupRepository.save(groupDocument);
  }

  private List<MemberDto> groupMembers(final GroupDocument groupDocument) {
    return groupDocument.getMembers().stream()
        .filter(member -> member.getRemovedAt() == null)
        .map(member -> mapper.map(member, MemberDto.class))
        .collect(Collectors.toList());
  }

  private GroupDocument findById(String groupId) {
    return groupRepository.findById(groupId)
        .orElseThrow(() -> new GroupException(ErrorCode.GROUP_NOT_FOUND).logError());
  }

  private GroupDocument findByExtId(UUID groupExtId) {
    return groupRepository.findByExtId(groupExtId)
        .orElseThrow(() -> new GroupException(ErrorCode.GROUP_NOT_FOUND).logError());
  }
}
