package com.swifttech.domain.service;

import com.swifttech.domain.dto.ConversationCreateDto;
import com.swifttech.domain.dto.ConversationDto;
import com.swifttech.domain.dto.filter.ConversationFilterDto;
import java.util.List;


public interface ConversationService {

  ConversationDto findOrCreate(ConversationCreateDto conversationCreateDto);

  List<ConversationDto> findConversations(ConversationFilterDto conversationFilterDto);
}
