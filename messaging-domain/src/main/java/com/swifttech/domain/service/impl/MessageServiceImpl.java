package com.swifttech.domain.service.impl;

import com.swifttech.domain.SecurityHolder;
import com.swifttech.domain.document.ConversationDocument;
import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.dto.ConversationCreateDto;
import com.swifttech.domain.dto.ConversationDto;
import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.dto.MessageDto;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import com.swifttech.domain.exception.ErrorCode;
import com.swifttech.domain.exception.MessageException;
import com.swifttech.domain.repository.ConversationRepository;
import com.swifttech.domain.repository.MessageRepository;
import com.swifttech.domain.service.ConversationService;
import com.swifttech.domain.service.MessageService;
import com.swifttech.domain.service.converter.ConversationConverter;
import com.swifttech.domain.service.converter.MessageConverter;
import com.swifttech.domain.service.validator.MessageValidator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

  private final MessageConverter messageConverter;
  private final ConversationConverter conversationConverter;

  private final MessageRepository messageRepository;
  private final ConversationRepository conversationRepository;

  private final MessageValidator messageValidator;
  private final MapperFacade mapper;
  private final ConversationService conversationService;
  private final SecurityHolder securityHolder;

  @Override
  public MessageDto store(final MessageCreateDto messageCreateDto) {
    messageValidator.validateStoreMessage(messageCreateDto);

    ConversationDto conversationDto = conversationService.findOrCreate(
        ConversationCreateDto.builder()
            .userId(securityHolder.getContextUserId())
            .groupId(messageCreateDto.getGroupId())
            .interlocutorId(messageCreateDto.getReceiverId())
            .build());

    MessageDocument messageDocument = messageConverter.convertToDocument(messageCreateDto);
    messageDocument.setConversationId(conversationDto.getConversationId());
    messageDocument.setSenderId(securityHolder.getContextUserId());

    messageDocument = messageRepository.save(messageDocument);

    return messageConverter.convertToDto(messageDocument);
  }

  @Override
  public List<MessageDto> findDirect(final String receiverId, final MessageFilterDto filter) {
    final ConversationDocument conversationDocument = conversationRepository
        .findByUserIdAndInterlocutorId(securityHolder.getContextUserId(), receiverId)
        .orElseThrow(() -> new MessageException(ErrorCode.CONVERSATION_NOT_FOUND).logWarn());

    List<MessageDocument> messageDocuments = messageRepository
        .findByConversationId(conversationDocument.getConversationId(), filter);

    return messageDocuments.stream()
        .map(messageConverter::convertToDto)
        .collect(Collectors.toList());
  }

  @Override
  public List<MessageDto> findGroup(final String groupId, final MessageFilterDto filter) {
    final ConversationDocument conversationDocument = conversationRepository
        .findByUserIdAndGroupId(securityHolder.getContextUserId(), groupId)
        .orElseThrow(() -> new MessageException(ErrorCode.CONVERSATION_NOT_FOUND).logWarn());

    List<MessageDocument> messageDocuments = messageRepository
        .findByConversationId(conversationDocument.getConversationId(), filter);

    return messageDocuments.stream()
        .map(messageConverter::convertToDto)
        .collect(Collectors.toList());
  }

  @Override
  public void send(MessageDto messageDto) {

  }
}
