package com.swifttech.domain.service.impl;

import com.swifttech.domain.SecurityHolder;
import com.swifttech.domain.document.ConversationDocument;
import com.swifttech.domain.dto.ConversationCreateDto;
import com.swifttech.domain.dto.ConversationDto;
import com.swifttech.domain.dto.filter.ConversationFilterDto;
import com.swifttech.domain.exception.ConversationException;
import com.swifttech.domain.exception.ErrorCode;
import com.swifttech.domain.repository.ConversationRepository;
import com.swifttech.domain.service.ConversationService;
import com.swifttech.domain.service.converter.ConversationConverter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConversationServiceImpl implements ConversationService {

  private final ConversationRepository conversationRepository;
  private final ConversationConverter conversationConverter;
  private final SecurityHolder securityHolder;

  @Override
  public ConversationDto findOrCreate(final ConversationCreateDto conversationCreateDto) {
    Optional<ConversationDocument> conversationDocument;
    ConversationDocument newConversationDocument;

    // for GROUP conversations
    if (StringUtils.isNoneEmpty(conversationCreateDto.getGroupId())) {
      conversationDocument = conversationRepository
          .findByUserIdAndGroupId(securityHolder.getContextUserId(), conversationCreateDto.getGroupId());
      if (conversationDocument.isPresent()) {
        return conversationConverter.convertToDto(conversationDocument.get());
      }

      newConversationDocument = conversationConverter.convertToDocument(conversationCreateDto);
      conversationRepository.findByGroupId(conversationCreateDto.getGroupId())
          .ifPresent(document -> newConversationDocument.setConversationId(document.getConversationId()));

      final ConversationDocument savedConversationDocument = conversationRepository.save(newConversationDocument);
      return conversationConverter.convertToDto(savedConversationDocument);
    }

    // for DIRECT conversation
    if (StringUtils.isNoneEmpty(conversationCreateDto.getInterlocutorId())) {
      conversationDocument = conversationRepository
          .findByUserIdAndInterlocutorId(securityHolder.getContextUserId(), conversationCreateDto.getInterlocutorId());
      if (conversationDocument.isPresent()) {
        return conversationConverter.convertToDto(conversationDocument.get());
      }

      newConversationDocument = conversationConverter.convertToDocument(conversationCreateDto);
      conversationRepository
          .findByUserIdAndInterlocutorId(conversationCreateDto.getInterlocutorId(), securityHolder.getContextUserId())
          .ifPresent(document -> newConversationDocument.setConversationId(document.getConversationId()));

      final ConversationDocument savedConversationDocument = conversationRepository.save(newConversationDocument);
      return conversationConverter.convertToDto(savedConversationDocument);
    }

    throw new ConversationException(ErrorCode.CONVERSATION_INAPPROPRIATE_PARAMS).logError();
  }

  @Override
  public List<ConversationDto> findConversations(final ConversationFilterDto filter) {
    final List<ConversationDocument> conversationDocuments = conversationRepository
        .findByUserId(securityHolder.getContextUserId());
    return conversationDocuments.stream()
        .map(conversationConverter::convertToDto)
        .collect(Collectors.toList());

  }
}
