package com.swifttech.domain.service;

import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.dto.MessageDto;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import java.util.List;

public interface MessageService {

  List<MessageDto> findDirect(String receiverId, MessageFilterDto messageFilterDto);

  List<MessageDto> findGroup(String groupId, MessageFilterDto messageFilterDto);

  MessageDto store(MessageCreateDto messageCreateDto);

  void send(MessageDto messageDto);

}
