package com.swifttech.domain.service;

import com.swifttech.domain.document.type.MemberType;
import com.swifttech.domain.dto.GroupCreateDto;
import com.swifttech.domain.dto.GroupDto;
import com.swifttech.domain.dto.MemberDto;
import java.util.List;
import java.util.UUID;

public interface GroupService {

  GroupDto createGroup(GroupCreateDto groupCreateDto);

  void addMember(String groupId, String userId, MemberType memberType);

  void addMember(UUID groupExtId, String userId, MemberType memberType);

  void removeMember(UUID groupExtId, String userId);

  void removeMember(String groupId, String userId);

  List<MemberDto> groupMembers(String groupId);

  List<MemberDto> groupMembers(UUID groupExtId);
}
