package com.swifttech.domain.helper;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
public class ConversationHelper {

  /**
   * Generate unique conversation id for 2 users. Used for DIRECT messages only.
   *
   * @param userA sender or receiver user id
   * @param userB sender or receiver user id
   * @return concatenated conversation id
   */
  public String generateDirectConversationId(final String userA, final String userB) {
    if (userA.compareTo(userB) < 0) {
      return String.join("|", userA, userB);
    }
    return String.join("|", userB, userA);
  }

  /**
   * Generate unique conversation for group.
   *
   * @param groupId group unique identifier
   * @return concatenated conversation id
   */
  public String generateGroupConversationId(final String groupId) {
    return "group:" + groupId;
  }

//  /**
//   * Get conversation id by interlocutor id and conversation type.
//   *
//   * @param interlocutorId the interlocutor id
//   * @param userId the user id
//   * @param conversationType {@link ConversationType} the conversation type
//   * @return conversation id
//   */
//  public String getConversationIdByInterlocutorId(final String interlocutorId, final String userId,
//      final ConversationType conversationType) {
//    return conversationType == ConversationType.DIRECT_CONVERSATION
//        ? generateDirectConversationId(interlocutorId, userId)
//        : generateGroupConversationId(interlocutorId);
//  }

  /**
   * Obtains receiver id from conversation id.
   *
   * @param conversationId the conversationId
   * @return receiver id
   */
  public String obtainReceiverId(final String userId, final String conversationId) {
    int index = conversationId.indexOf(userId);
    if (index == 0) {
      return conversationId.substring(userId.length() + 1);
    } else if (index > 0) {
      return conversationId.substring(0, index - 1);
    }
    return "";
  }

  /**
   * Obtains group id conversation id.
   *
   * @param conversationId conversation id
   * @return group
   */
  public String obtainGroupId(final String conversationId) {
    return StringUtils.removeStart(conversationId, "group:");
  }
}
