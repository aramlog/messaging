package com.swifttech.domain.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseException extends RuntimeException {

  private final ErrorCode errorCode;
  private final String message;

  public BaseException(ErrorCode errorCode, String message) {
    super(message);
    this.message = message;
    this.errorCode = errorCode;
  }

  public BaseException(ErrorCode errorCode) {
    super(errorCode.getDefaultMessage());
    this.errorCode = errorCode;
    this.message = errorCode.getDefaultMessage();
  }

  public BaseException logInfo() {
    log.info(this.message);
    return this;
  }

  public BaseException logDebug() {
    log.debug(this.message);
    return this;
  }

  public BaseException logWarn() {
    log.warn(this.message);
    return this;
  }

  public BaseException logError() {
    log.error(this.message);
    return this;
  }
}
