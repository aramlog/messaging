package com.swifttech.domain.exception;

public class MessageException extends BaseException {

  public MessageException(ErrorCode errorCode, String message) {
    super(errorCode, message);
  }

  public MessageException(ErrorCode errorCode) {
    super(errorCode);
  }
}
