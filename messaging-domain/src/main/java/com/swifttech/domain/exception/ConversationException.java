package com.swifttech.domain.exception;

public class ConversationException extends BaseException {

  public ConversationException(ErrorCode errorCode, String message) {
    super(errorCode, message);
  }

  public ConversationException(ErrorCode errorCode) {
    super(errorCode);
  }
}
