package com.swifttech.domain.exception;

public class GroupException extends BaseException {

  public GroupException(ErrorCode errorCode, String message) {
    super(errorCode, message);
  }

  public GroupException(ErrorCode errorCode) {
    super(errorCode);
  }
}
