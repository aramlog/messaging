package com.swifttech.domain.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCode {

  MESSAGE_NOT_FOUND(HttpStatus.NOT_FOUND, "Message not found."),
  MESSAGE_INVALID(HttpStatus.BAD_REQUEST, "Message is invalid."),
  MESSAGE_NOT_PROVIDED(HttpStatus.BAD_REQUEST, "Message payload is incorrect."),
  MESSAGE_INAPPROPRIATE_SENDER(HttpStatus.BAD_REQUEST, "Message sender is inappropriate."),

  CONVERSATION_INAPPROPRIATE_PARAMS(HttpStatus.BAD_REQUEST, "Either group or interlocutor id must be provided."),
  CONVERSATION_NOT_FOUND(HttpStatus.NOT_FOUND, "Unable to find appropriate conversation."),

  GROUP_NOT_FOUND(HttpStatus.NOT_FOUND, "Group not found."),
  GROUP_EXT_ID_DUPLICATED(HttpStatus.NOT_FOUND, "Group external id duplicated."),
  GROUP_MEMBER_NOT_FOUND(HttpStatus.NOT_FOUND, "Group member not found.");

  private final HttpStatus httpStatus;
  private final String defaultMessage;
}
