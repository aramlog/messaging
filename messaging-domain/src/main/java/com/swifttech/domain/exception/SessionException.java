package com.swifttech.domain.exception;

public class SessionException extends BaseException {

  public SessionException(ErrorCode errorCode, String message) {
    super(errorCode, message);
  }

  public SessionException(ErrorCode errorCode) {
    super(errorCode);
  }
}
