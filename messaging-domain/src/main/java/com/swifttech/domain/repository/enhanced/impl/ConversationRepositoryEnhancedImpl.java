package com.swifttech.domain.repository.enhanced.impl;

import com.swifttech.domain.repository.enhanced.ConversationRepositoryEnhanced;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ConversationRepositoryEnhancedImpl implements ConversationRepositoryEnhanced {

  private final MongoTemplate mongoTemplate;

}
