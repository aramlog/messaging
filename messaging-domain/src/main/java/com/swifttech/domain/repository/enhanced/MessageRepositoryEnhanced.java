package com.swifttech.domain.repository.enhanced;

import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import java.util.List;

public interface MessageRepositoryEnhanced {

  /**
   * The messages of the specified conversation id.
   *
   * @param conversationId the user unique identifier
   * @param messageFilterDto {@link MessageFilterDto} criteria
   * @return the list of {@link MessageDocument}
   */
  List<MessageDocument> findByConversationId(String conversationId,  MessageFilterDto messageFilterDto);

}
