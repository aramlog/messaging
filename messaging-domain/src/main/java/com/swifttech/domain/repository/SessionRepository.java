package com.swifttech.domain.repository;

import com.swifttech.domain.document.SessionDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends MongoRepository<SessionDocument, String> {

}
