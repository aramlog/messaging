package com.swifttech.domain.repository;

import com.swifttech.domain.document.GroupDocument;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends MongoRepository<GroupDocument, String> {

  Optional<GroupDocument> findByExtId(UUID groupId);
}
