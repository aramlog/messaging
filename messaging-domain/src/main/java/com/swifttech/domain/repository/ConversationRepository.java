package com.swifttech.domain.repository;

import com.swifttech.domain.document.ConversationDocument;
import com.swifttech.domain.repository.enhanced.ConversationRepositoryEnhanced;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversationRepository extends
    MongoRepository<ConversationDocument, String>, ConversationRepositoryEnhanced {

  Optional<ConversationDocument> findByUserIdAndInterlocutorId(String userId, String interlocutorId);

  Optional<ConversationDocument> findByUserIdAndGroupId(String userId, String groupId);

  Optional<ConversationDocument> findByGroupId(String groupId);

  List<ConversationDocument> findByUserId(String userId);
}
