package com.swifttech.domain.repository;

import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.repository.enhanced.MessageRepositoryEnhanced;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends MongoRepository<MessageDocument, String>, MessageRepositoryEnhanced {

}
