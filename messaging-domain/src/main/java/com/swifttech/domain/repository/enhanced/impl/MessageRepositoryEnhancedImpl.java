package com.swifttech.domain.repository.enhanced.impl;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import com.swifttech.domain.document.MessageDocument;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import com.swifttech.domain.enums.MessageDirection;
import com.swifttech.domain.enums.MessageSort;
import com.swifttech.domain.repository.enhanced.MessageRepositoryEnhanced;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class MessageRepositoryEnhancedImpl implements MessageRepositoryEnhanced {

  private final MongoTemplate mongoTemplate;

  private static final String ID = "id";
  private static final String SENDER_ID = "senderId";
  private static final String RECEIVER_ID = "receiverId";
  private static final String CONVERSATION_ID = "conversationId";
  private static final String MESSAGE_TYPE = "type";
  private static final String VIEWS = "views";
  private static final String MESSAGE_DATE = "date";

  @Override
  public List<MessageDocument> findByConversationId(final String conversationId, final MessageFilterDto filter) {
    final Criteria criteria = where(CONVERSATION_ID).is(conversationId);

    ObjectId messageId;
    if (filter.getPivotMessageId() != null) {
      messageId = new ObjectId(filter.getPivotMessageId());
      if (filter.getDirection() == MessageDirection.UP) {
        if (BooleanUtils.isTrue(filter.getIncludedPivotMessage())) {
          criteria.and(ID).gte(messageId);
        } else {
          criteria.and(ID).gt(messageId);
        }
      } else if (filter.getDirection() == MessageDirection.DOWN) {
        messageId = new ObjectId(filter.getPivotMessageId());
        if (BooleanUtils.isTrue(filter.getIncludedPivotMessage())) {
          criteria.and(ID).lte(messageId);
        } else {
          criteria.and(ID).lt(messageId);
        }
      }
    }

    final Query query = new Query();
    query.addCriteria(criteria).limit(filter.getLimit() == null ? 50 : filter.getLimit());

    if (filter.getSort() != null) {
      if (filter.getSort() == MessageSort.DESC) {
        query.with(Sort.by(Sort.Direction.DESC, MESSAGE_DATE));
      } else if (filter.getSort() == MessageSort.ASC) {
        query.with(Sort.by(Sort.Direction.ASC, MESSAGE_DATE));
      }
    }

    return mongoTemplate.find(query, MessageDocument.class);
  }
}
