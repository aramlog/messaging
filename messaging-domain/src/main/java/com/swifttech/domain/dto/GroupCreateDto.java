package com.swifttech.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GroupCreateDto")
public class GroupCreateDto {

  @ApiModelProperty(value = "Group external id", example = "2462cbca-c970-4ec7-afac-d01fc2b8d0e3")
  private UUID extId;

  @ApiModelProperty(value = "Group name", example = "My Group")
  private String name;

  @ApiModelProperty(value = "Group description", example = "Group description")
  private String description;

  @ApiModelProperty(value = "Group picture")
  private String picture;
}
