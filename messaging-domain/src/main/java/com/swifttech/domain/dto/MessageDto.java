package com.swifttech.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.swifttech.domain.document.type.MessageType;
import com.swifttech.domain.util.Pattern;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "MessageDto")
public class MessageDto {

  @NotBlank
  @ApiModelProperty(value = "The message auto generated id")
  private String id;

  @NotBlank
  @ApiModelProperty(value = "The message  external id")
  private String extId;

  @NotNull
  @ApiModelProperty(value = "The message type", allowableValues = "DIRECT, GROUP")
  private MessageType type;

  @NotBlank
  @ApiModelProperty(value = "The sender user id")
  private String senderId;

  @ApiModelProperty(value = "The receiver user id")
  private String receiverId;

  @ApiModelProperty(value = "The group id")
  private String groupId;

  @ApiModelProperty(value = "The text message")
  private String text;

  @ApiModelProperty(value = "The image message")
  private String image;

  @ApiModelProperty(value = "The count of views")
  private int viewCount;

  @ApiModelProperty(value = "The count of likes")
  private int likeCount;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Pattern.DATE_TIME_PATTERN)
  @ApiModelProperty(value = "The message sent datetime")
  private LocalDateTime sentAt;

  @ApiModelProperty(value = "Indicate if message is deleted")
  private boolean deleted;

}
