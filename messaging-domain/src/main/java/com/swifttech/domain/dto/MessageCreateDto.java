package com.swifttech.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MessageCreateDto")
public class MessageCreateDto {

  @ApiModelProperty(value = "External id of the message")
  private String extId;

  @ApiModelProperty(value = "The user id of receiver")
  private String receiverId;

  @ApiModelProperty(value = "The group id")
  private String groupId;

  @ApiModelProperty(value = "The text message")
  private String text;

  @ApiModelProperty(value = "The image message")
  private String image;

  @ApiModelProperty(value = "The message header")
  private Map<String, String> header;

}
