package com.swifttech.domain.dto.filter;

import com.swifttech.domain.enums.MessageDirection;
import com.swifttech.domain.enums.MessageSort;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "MessageFilterDto")
public class MessageFilterDto {

  @ApiModelProperty(value = "The pivot message id")
  private String pivotMessageId;

  @ApiModelProperty(value = "Messages direction")
  private MessageDirection direction;

  @ApiModelProperty(value = "Indicate that pivotal message is included in response")
  private Boolean includedPivotMessage;

  @ApiModelProperty(value = "The messages limit", example = "50", notes = "Default 50 messages")
  private Integer limit;

  @ApiModelProperty(value = "Sort messages", example = "DESC", allowableValues = "DESC, ASC")
  private MessageSort sort;
}
