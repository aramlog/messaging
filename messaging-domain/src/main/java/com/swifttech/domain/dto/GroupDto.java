package com.swifttech.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GroupDto")
public class GroupDto {

  @ApiModelProperty(value = "Group id")
  private String id;

  @ApiModelProperty(value = "Group external id")
  private UUID extId;

  @ApiModelProperty(value = "Group name")
  private String name;

  @ApiModelProperty(value = "Group description")
  private String description;

  @ApiModelProperty(value = "Group picture")
  private String picture;

  @ApiModelProperty(value = "Group members count")
  private int membersCount;
}
