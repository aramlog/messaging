package com.swifttech.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConversationCreateDto {

  private String userId;
  private String interlocutorId;
  private String groupId;
}
