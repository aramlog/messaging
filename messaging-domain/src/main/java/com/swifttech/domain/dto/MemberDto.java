package com.swifttech.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.swifttech.domain.document.type.MemberType;
import com.swifttech.domain.util.Pattern;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class MemberDto {

  private MemberType type;

  private String userId;

  private String addedBy;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Pattern.DATE_TIME_PATTERN)
  private LocalDateTime addedAt;

}
