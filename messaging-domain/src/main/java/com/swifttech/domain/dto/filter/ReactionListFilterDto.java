package com.swifttech.domain.dto.filter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "ReactionListFilterDto")
public class ReactionListFilterDto {

  @ApiModelProperty(value = "Offset/skip")
  private int skip;

  @ApiModelProperty(value = "Limit")
  private int limit;
}
