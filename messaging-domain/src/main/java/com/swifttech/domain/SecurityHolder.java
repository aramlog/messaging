package com.swifttech.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class SecurityHolder {

  private final ThreadLocal<ContextUser> contextUser = new ThreadLocal<>();

  public void setContextUser() {
    String userId = String.valueOf(Thread.currentThread().getId());
    contextUser.set(new ContextUser(userId, null));
  }

  public ContextUser getContextUser() {
    return contextUser.get();
  }

  public String getContextUserId() {
    return "mike@";
    //return contextUser.get().getId();
  }

  @Data
  @AllArgsConstructor
  class ContextUser {
    private String id;
    private String token;
  }
}