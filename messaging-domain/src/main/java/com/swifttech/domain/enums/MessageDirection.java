package com.swifttech.domain.enums;

public enum MessageDirection {
  UP, DOWN
}
