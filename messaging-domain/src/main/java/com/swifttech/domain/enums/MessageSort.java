package com.swifttech.domain.enums;

public enum MessageSort {
  DESC, ASC
}
