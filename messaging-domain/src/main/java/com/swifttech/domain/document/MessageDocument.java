package com.swifttech.domain.document;

import com.swifttech.domain.document.embedded.ActivityEmbedded;
import com.swifttech.domain.document.embedded.BodyEmbedded;
import com.swifttech.domain.document.embedded.HeaderEmbedded;
import com.swifttech.domain.document.embedded.ReactionEmbedded;
import com.swifttech.domain.document.embedded.ViewEmbedded;
import com.swifttech.domain.document.type.MessageType;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "im_messages")
public class MessageDocument {

  @Id
  private String id;

  @NotNull
  @Indexed(unique = true)
  @Field(value = "ext_id")
  private String extId;

  @NotNull
  @Field(value = "type")
  private MessageType type;

  @NotNull
  @Field(value = "sender_id")
  private String senderId;

  @Field(value = "receiver_id")
  private String receiverId;

  @Field(value = "group_id")
  private String groupId;

  @NotNull
  @Field(value = "conversation_id")
  private String conversationId;

  @Field(value = "header")
  private Set<HeaderEmbedded> header;

  @NotNull
  @Field(value = "body")
  private BodyEmbedded body;

  @Field(value = "views")
  private Set<ViewEmbedded> views;

  @Field(value = "reactions")
  private List<ReactionEmbedded> reactions;

  @Field(value = "deleted")
  private boolean deleted;

  @NotNull
  @Field(value = "updated_at")
  private LocalDateTime updatedAt;

  @NotNull
  @Field(value = "created_at")
  private LocalDateTime createdAt;

  @Field(value = "activities")
  private List<ActivityEmbedded> activities;

  public Set<ViewEmbedded> getViews() {
    return views == null ? new HashSet<>() : views;
  }

  public List<ReactionEmbedded> getReactions() {
    return reactions == null ? new ArrayList<>() : reactions;
  }
}
