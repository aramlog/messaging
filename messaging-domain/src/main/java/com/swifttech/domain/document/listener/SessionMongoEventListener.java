package com.swifttech.domain.document.listener;

import com.swifttech.domain.document.SessionDocument;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class SessionMongoEventListener extends AbstractMongoEventListener<SessionDocument> {

}
