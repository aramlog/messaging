package com.swifttech.domain.document.type;

public enum MemberType {
  ADMIN,
  PARTICIPANT
}
