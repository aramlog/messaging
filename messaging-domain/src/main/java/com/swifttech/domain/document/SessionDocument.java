package com.swifttech.domain.document;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "sessions")
public class SessionDocument {

  @NotNull
  @Field(value = "connection_id")
  private String connectionId;

  @NotNull
  @Field(value = "user_id")
  private String userId;

  @Field(value = "connected")
  private boolean connected;

  @Field(value = "access_token")
  private String accessToken;

  @Field(value = "last_connected_at")
  private LocalDateTime lastConnectedAt;

  @Field(value = "last_disconnected_at")
  private LocalDateTime lastDisconnectedAt;
}
