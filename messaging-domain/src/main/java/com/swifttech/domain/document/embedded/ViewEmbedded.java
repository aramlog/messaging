package com.swifttech.domain.document.embedded;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ViewEmbedded {

  @Field("user_id")
  private String userId;

  @Field("seen_at")
  private LocalDateTime seenAt;
}
