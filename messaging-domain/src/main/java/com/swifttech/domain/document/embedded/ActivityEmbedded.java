package com.swifttech.domain.document.embedded;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityEmbedded {

  @Field("field_name")
  private String fieldName;

  @Field("previous_value")
  private String previousValue;

  @Field("current_value")
  private String currentValue;

  @Field("action")
  private String action;

  @Field("changed_by")
  private String changedBy;

  @Field("changed_at")
  private LocalDateTime changedAt;
}
