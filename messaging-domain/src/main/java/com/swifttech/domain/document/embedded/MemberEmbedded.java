package com.swifttech.domain.document.embedded;

import com.swifttech.domain.document.type.MemberType;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberEmbedded implements Serializable, Comparable<MemberEmbedded> {

  @Field("type")
  private MemberType type;

  @Field("user_id")
  private String userId;

  @Field("added_by")
  private String addedBy;

  @Field("removed_by")
  private String removedBy;

  @Field("added_at")
  private LocalDateTime addedAt;

  @Field("removed_at")
  private LocalDateTime removedAt;

  @Override
  public int compareTo(MemberEmbedded member) {
    return this.addedAt.isAfter(member.addedAt) ? 1 : -1;
  }

  public static MemberEmbedded asSelfAdmin(String userId) {
    return MemberEmbedded.builder()
        .type(MemberType.ADMIN)
        .userId(userId)
        .addedBy(userId)
        .addedAt(LocalDateTime.now())
        .build();
  }

  public static MemberEmbedded asAdmin(String userId, String addedBy) {
    return MemberEmbedded.builder()
        .type(MemberType.ADMIN)
        .userId(userId)
        .addedBy(addedBy)
        .addedAt(LocalDateTime.now())
        .build();
  }

  public static MemberEmbedded asParticipant(String userId, String addedBy) {
    return MemberEmbedded.builder()
        .type(MemberType.PARTICIPANT)
        .userId(userId)
        .addedBy(addedBy)
        .addedAt(LocalDateTime.now())
        .build();
  }
}
