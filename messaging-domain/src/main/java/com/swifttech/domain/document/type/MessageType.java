package com.swifttech.domain.document.type;

public enum MessageType {
  DIRECT,
  GROUP
}

