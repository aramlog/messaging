package com.swifttech.domain.document.embedded;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HeaderEmbedded {

  @Field("key")
  @NotBlank
  private String key;

  @Field("value")
  @NotBlank
  private String value;
}
