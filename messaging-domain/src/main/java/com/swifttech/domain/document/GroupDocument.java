package com.swifttech.domain.document;

import com.swifttech.domain.document.embedded.ActivityEmbedded;
import com.swifttech.domain.document.embedded.MemberEmbedded;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "im_groups")
public class GroupDocument {

  @Id
  private String id;

  @NotNull
  @Indexed(unique = true)
  @Field(value = "ext_id")
  private UUID extId;

  @Field("name")
  private String name;

  @Field("description")
  private String description;

  @Field("picture")
  private String picture;

  @Field("members")
  private Set<MemberEmbedded> members;

  @Field("created_by")
  private String createdBy;

  @Field("created_at")
  private LocalDateTime createdAt;

  @Field("updated_at")
  private LocalDateTime updatedAt;

  @Field(value = "activities")
  private List<ActivityEmbedded> activities;

  public void addMember(MemberEmbedded member) {
    if (members == null) {
      members = new TreeSet<>();
    }
    members.add(member);
  }
}
