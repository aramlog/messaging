package com.swifttech.domain.document.listener;

import com.swifttech.domain.document.GroupDocument;
import java.time.LocalDateTime;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

public class GroupMongoEventListener extends AbstractMongoEventListener<GroupDocument> {

  @Override
  public void onBeforeConvert(BeforeConvertEvent<GroupDocument> event) {
  }

  @Override
  public void onBeforeSave(BeforeSaveEvent event) {
    GroupDocument groupDocument = (GroupDocument) event.getSource();
    if (groupDocument.getCreatedAt() == null) {
      ((GroupDocument)event.getSource()).setCreatedAt(LocalDateTime.now());
    }
  }

}
