package com.swifttech.domain.document;

import com.swifttech.domain.document.embedded.OptionsEmbedded;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "im_conversations")
public class ConversationDocument {

  @Id
  private String id;

  @NotNull
  @Field("conversation_id")
  private String conversationId;

  @NotNull
  @Field("user_id")
  private String userId;

  @Field("interlocutor_id")
  private String interlocutorId;

  @Field("group_id")
  private String groupId;

//  @Field("last_message")
//  private MessageDocument lastMessage;

  @Field("total_count")
  private int totalCount;

  @Field("unread_count")
  private int unreadCount;

  @Field("archived")
  private boolean archived;

  @Field("options")
  private OptionsEmbedded options;

  @Field("created_at")
  private LocalDateTime createdAt;

  @Field("updated_at")
  private LocalDateTime updatedAt;
}
