package com.swifttech.domain.document.embedded;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OptionsEmbedded {

  @Field("is_muted")
  private boolean isMuted;

  @Field("muted_until")
  private LocalDateTime mutedUntil;
}
