package com.swifttech.domain.document.embedded;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.StringUtils;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BodyEmbedded {

  @Field("content")
  private String content;

  @Field("image")
  private String image;

  private boolean validate() {
    return !StringUtils.isEmpty(content) || !StringUtils.isEmpty(image);
  }
}
