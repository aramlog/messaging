package com.swifttech.domain.document.listener;

import com.swifttech.domain.document.ConversationDocument;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class ConversationMongoEventListener extends AbstractMongoEventListener<ConversationDocument> {

}
