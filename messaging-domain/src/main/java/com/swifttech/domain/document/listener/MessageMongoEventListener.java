package com.swifttech.domain.document.listener;

import com.swifttech.domain.document.MessageDocument;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;

public class MessageMongoEventListener extends AbstractMongoEventListener<MessageDocument> {

}
