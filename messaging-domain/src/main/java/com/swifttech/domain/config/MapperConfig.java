package com.swifttech.domain.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.swifttech.domain.document.GroupDocument;
import com.swifttech.domain.dto.GroupDto;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

  @Bean
  public MapperFacade mapper() {
    final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    mapperFactory.classMap(GroupDocument.class, GroupDto.class)
        .customize(new CustomMapper<GroupDocument, GroupDto>() {
          @Override
          public void mapAtoB(GroupDocument groupDocument, GroupDto groupDto, MappingContext context) {
            groupDto.setMembersCount(groupDocument.getMembers().size());
          }
        })
        .byDefault()
        .register();
    
    return mapperFactory.getMapperFacade();
  }

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper().registerModule(new JavaTimeModule());
  }
}
