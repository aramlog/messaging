package com.swifttech.domain.config;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.swifttech.domain.document.listener.ConversationMongoEventListener;
import com.swifttech.domain.document.listener.GroupMongoEventListener;
import com.swifttech.domain.document.listener.MessageMongoEventListener;
import com.swifttech.domain.document.listener.SessionMongoEventListener;
import lombok.AllArgsConstructor;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

@Configuration
@AllArgsConstructor
public class MongoConfig {

  private final MongoDbFactory mongoDbFactory;
  private final MongoClient mongoClient;
  private final MongoMappingContext mongoMappingContext;

  @Bean
  public MongoDatabase mongoDatabase() {
    return mongoClient.getDatabase("messaging").withCodecRegistry(getCodecRegistry());
  }

  @Bean
  public MappingMongoConverter mappingMongoConverter() {
    DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
    MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
    converter.setTypeMapper(new DefaultMongoTypeMapper(null));

    return converter;
  }

  @Bean
  public static CodecRegistry getCodecRegistry() {
    CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
    return fromRegistries(MongoClient.getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
  }

  @Bean
  public GroupMongoEventListener groupMongoEventListener() {
    return new GroupMongoEventListener();
  }

  @Bean
  public MessageMongoEventListener messageMongoEventListener() {
    return new MessageMongoEventListener();
  }

  @Bean
  public ConversationMongoEventListener conversationMongoEventListener() {
    return new ConversationMongoEventListener();
  }

  @Bean
  public SessionMongoEventListener sessionMongoEventListener() {
    return new SessionMongoEventListener();
  }
}
