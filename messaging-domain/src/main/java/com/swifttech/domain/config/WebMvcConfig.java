package com.swifttech.domain.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig {

//  private final CorsConfigProperties corsConfigProperties;

  @Bean
  public WebMvcConfigurer config() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping(corsConfigProperties.getMapping())
//            .allowedMethods(corsConfigProperties.getAllowedMethods())
//            .allowedOrigins(corsConfigProperties.getAllowedOrigins())
//            .allowedHeaders(corsConfigProperties.getAllowedHeaders());
      }
    };
  }

}
