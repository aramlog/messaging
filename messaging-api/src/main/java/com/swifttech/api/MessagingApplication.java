package com.swifttech.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = "com.swifttech")
@EnableMongoRepositories(basePackages = "com.swifttech.domain.repository")
public class MessagingApplication {

  public static void main(String[] args) {
    new SpringApplication(MessagingApplication.class).run(args);
  }
}

