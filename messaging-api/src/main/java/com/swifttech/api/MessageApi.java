package com.swifttech.api;

import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.dto.MessageDto;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "Messages")
@RequestMapping("api/messages")
public interface MessageApi {

  @ApiOperation(value = "Store a new message")
  @PostMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<MessageDto> storeMessage(
      @ApiParam(value = "Request body for storing message", required = true)
      @RequestBody @Valid MessageCreateDto messageCreateDto);

  @ApiOperation(value = "Find direct messages")
  @GetMapping(value = "/direct/{receiverId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<List<MessageDto>> findDirectMessages(
      @PathVariable String receiverId, MessageFilterDto filterDto);

  @ApiOperation(value = "Find group messages")
  @GetMapping(value = "/group/{groupId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<List<MessageDto>> findGroupMessages(
      @PathVariable String groupId, MessageFilterDto filterDto);
}
