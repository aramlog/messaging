package com.swifttech.api;

import com.swifttech.domain.dto.GroupCreateDto;
import com.swifttech.domain.dto.GroupDto;
import com.swifttech.domain.dto.MemberDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(tags = "Groups")
@RequestMapping("api/groups")
public interface GroupApi {

  @ApiOperation(value = "Create a new group")
  @PostMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<GroupDto> createGroup(
      @ApiParam(value = "Request body for creating group", required = true)
      @RequestBody @Valid GroupCreateDto groupCreateDto);

  @ApiOperation(value = "Add new admin to group")
  @PostMapping(value = "/{groupId}/members/admin/{userId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<Void> addAdminMember(@PathVariable String groupId, @PathVariable String userId);

  @ApiOperation(value = "Add new participant to group")
  @PostMapping(value = "/{groupId}/members/participant/{userId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<Void> addParticipantMember(@PathVariable String groupId, @PathVariable String userId);

  @ApiOperation(value = "Remove member from group")
  @DeleteMapping(value = "/{groupId}/members/{userId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<Void> removeMember(@PathVariable String groupId, @PathVariable String userId);

  @ApiOperation(value = "Get group members")
  @GetMapping(value = "/{groupId}/members", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  ResponseEntity<List<MemberDto>> getMembers(@PathVariable String groupId);

}
