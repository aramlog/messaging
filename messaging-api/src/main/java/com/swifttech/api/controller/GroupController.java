package com.swifttech.api.controller;

import com.swifttech.api.GroupApi;
import com.swifttech.domain.SecurityHolder;
import com.swifttech.domain.document.type.MemberType;
import com.swifttech.domain.dto.GroupCreateDto;
import com.swifttech.domain.dto.GroupDto;
import com.swifttech.domain.dto.MemberDto;
import com.swifttech.domain.service.GroupService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class GroupController implements GroupApi {

  private final GroupService groupService;
  private final SecurityHolder securityHolder;

  public ResponseEntity<GroupDto> createGroup(final GroupCreateDto groupCreateDto) {
    log.info("Crate a new group with the following details: {} ", groupCreateDto);
    securityHolder.setContextUser();
    return ResponseEntity.ok(groupService.createGroup(groupCreateDto));
  }

  @Override
  public ResponseEntity<Void> addAdminMember(final String groupId, final String userId) {
    log.info("Add a new admin with id: {} in group with id: {}", userId, groupId);
    securityHolder.setContextUser();
    groupService.addMember(groupId, userId, MemberType.ADMIN);
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<Void> addParticipantMember(final String groupId, final String userId) {
    log.info("Add a new participant with id: {} in group with id: {}", userId, groupId);
    securityHolder.setContextUser();
    groupService.addMember(groupId, userId, MemberType.PARTICIPANT);
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<Void> removeMember(final String groupId, final String userId) {
    log.info("Remove member with id: {} from group with id: {}", userId, groupId);
    securityHolder.setContextUser();
    groupService.removeMember(groupId, userId);
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<List<MemberDto>> getMembers(final String groupId) {
    log.info("Find members of group with id: {} ", groupId);
    securityHolder.setContextUser();
    return ResponseEntity.ok(groupService.groupMembers(groupId));
  }
}
