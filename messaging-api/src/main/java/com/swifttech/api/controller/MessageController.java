package com.swifttech.api.controller;

import com.swifttech.api.MessageApi;
import com.swifttech.domain.dto.MessageCreateDto;
import com.swifttech.domain.dto.MessageDto;
import com.swifttech.domain.dto.filter.MessageFilterDto;
import com.swifttech.domain.service.MessageService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController implements MessageApi {

  private final MessageService messageService;

  @Override
  public ResponseEntity<MessageDto> storeMessage(@Valid MessageCreateDto messageCreateDto) {
    log.info("Store a new message with the following details: {} ", messageCreateDto);
    return ResponseEntity.ok(messageService.store(messageCreateDto));
  }

  @Override
  public ResponseEntity<List<MessageDto>> findDirectMessages(String receiverId, MessageFilterDto filterDto) {
    log.info("Find direct messages for receiver id : {} ", receiverId);
    return ResponseEntity.ok(messageService.findDirect(receiverId, filterDto));
  }

  @Override
  public ResponseEntity<List<MessageDto>> findGroupMessages(String groupId, MessageFilterDto filterDto) {
    log.info("Find group messages for group id : {} ", groupId);
    return ResponseEntity.ok(messageService.findGroup(groupId, filterDto));
  }
}
