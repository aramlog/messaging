<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.swifttech</groupId>
  <artifactId>messaging</artifactId>
  <packaging>pom</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>Messaging</name>
  <description>Messaging application</description>

  <modules>
    <module>messaging-api</module>
    <module>messaging-aws</module>
    <module>messaging-config</module>
    <module>messaging-domain</module>
    <module>messaging-service</module>
  </modules>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.5.RELEASE</version>
  </parent>

  <properties>
    <java.version>1.8</java.version>
    <jjwt.version>0.9.1</jjwt.version>
    <swagger.version>2.9.2</swagger.version>
    <lombok.version>1.18.12</lombok.version>
    <hibernate.types.version>2.7.0</hibernate.types.version>
    <orika.version>1.5.4</orika.version>
    <commons.lang3.version>3.9</commons.lang3.version>
    <commons.collections.version>4.3</commons.collections.version>
    <commons.text.version>1.8</commons.text.version>
    <commons.io.version>2.6</commons.io.version>
    <amazon.s3.version>1.11.639</amazon.s3.version>
    <ehcache.version>3.8.1</ehcache.version>
  </properties>

  <dependencyManagement>

    <dependencies>

      <!-- Modules -->
      <dependency>
        <groupId>com.swifttech</groupId>
        <artifactId>messaging-api</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>com.swifttech</groupId>
        <artifactId>messaging-aws</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>com.swifttech</groupId>
        <artifactId>messaging-config</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>com.swifttech</groupId>
        <artifactId>messaging-domain</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>com.swifttech</groupId>
        <artifactId>messaging-service</artifactId>
        <version>${project.version}</version>
      </dependency>

      <!-- Swagger -->
      <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger2</artifactId>
        <version>${swagger.version}</version>
      </dependency>
      <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger-ui</artifactId>
        <version>${swagger.version}</version>
      </dependency>
      <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-bean-validators</artifactId>
        <version>${swagger.version}</version>
      </dependency>

      <!-- Lombok -->
      <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>${lombok.version}</version>
        <scope>provided</scope>
      </dependency>

      <!-- Orika -->
      <dependency>
        <groupId>ma.glasnost.orika</groupId>
        <artifactId>orika-core</artifactId>
        <version>${orika.version}</version>
      </dependency>

      <!-- JWT -->
      <dependency>
        <groupId>io.jsonwebtoken</groupId>
        <artifactId>jjwt</artifactId>
        <version>${jjwt.version}</version>
      </dependency>

      <!-- Apache -->
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>${commons.lang3.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-collections4</artifactId>
        <version>${commons.collections.version}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-text</artifactId>
        <version>${commons.text.version}</version>
      </dependency>
      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>${commons.io.version}</version>
      </dependency>

      <!-- AWS -->
      <dependency>
        <groupId>com.amazonaws</groupId>
        <artifactId>aws-java-sdk-s3</artifactId>
        <version>${amazon.s3.version}</version>
      </dependency>

      <!-- EhCache -->
      <dependency>
        <groupId>org.ehcache</groupId>
        <artifactId>ehcache</artifactId>
        <version>${ehcache.version}</version>
      </dependency>

    </dependencies>

  </dependencyManagement>

</project>